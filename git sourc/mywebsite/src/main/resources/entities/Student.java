@entity
@table(name="student",catalog="website",appliesTo="")
public class Student {
	private static final long serialVersionUID=1L;
	
	private long id;
	private String firstname;
	private String lastname;
	private Integer age;
	
	
	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="ID",unique true,nullable=false)
	public long getID() {
		return id;
	}
	public void setId(long id) {
		this.id=id;
	}
	
	@Column(name="firstname", length=20)
	public long getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname=firstname;
	}
	@Colum(name="lastname",length=20)
	public long getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname=lastname;
	}
	@Colum(name="age",length=20)
	public long getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age=age;
	}
}
