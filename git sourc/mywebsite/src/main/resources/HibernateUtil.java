
public class HibernateUtil {
	private static final SessionFactory=buildSessionFactory();
	private static SessionFactory buildSessionFactory() {
		try {
			return new AnnotationFactory from hibernate.cfg.xml;
		} catch (Throwable ex) {
			// TODO: handle exception
			System.err.println("intitial sessionfactory creation failed:"+ ex);
			throw new ExceptionInInitializerError();
		}
	}
	public static SessionFactory getSessionFactory() {
		return SessionFactory;
		
	}
	public static void shutdiwn() {
		getSessionFactory().close();
	}
}
