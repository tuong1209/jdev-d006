package com.website.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.website.springmvc.entities.Course;
import com.website.springmvc.entities.Student;
import com.website.springmvc.service.StudentService;

@Controller
@RequestMapping(value = "/controller")
public class studentController {
	@Autowired
	private StudentService studentService;
	
	
	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public ModelAndView getAllStudent() {
		ModelAndView model = new ModelAndView();
		model.setViewName("student");
		model.addObject("students", studentService.getAll());

		return model;
	}

	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ModelAndView getStudentById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("studentDetail");
		model.addObject("mode", mode);
		model.addObject("student", studentService.get(id));
		return model;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveStudent(@ModelAttribute("student") Student student) {
		if (student.getId() == null) {
			studentService.add(student);
		} else {
			studentService.update(student);
		}

		return "redirect:/controller/student";
	}

	@RequestMapping(value = "/addStudent", method = RequestMethod.GET)
	public ModelAndView addStudent() {
		ModelAndView model = new ModelAndView();

		model.setViewName("studentDetail");
		model.addObject("student", new Student());
		//model.addObject("mode", "EDIT");

		return model;
	}
	
	@RequestMapping(value = "/student/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id) {		
		studentService.delete(id);
	}
}
