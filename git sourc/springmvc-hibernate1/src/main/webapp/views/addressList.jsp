<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/address.js" />"></script>
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			list address
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>id</th>
						<th>street</th>
						<th>district</th>
						<th>city</th>
						<th>acction</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty address}">
							<c:forEach items="${address}" var="address">
								<tr>
									<td>${address.id}</td>
									<td>${address.street}</td>
									<td>${address.district}</td>
									<td>${address.city}</td>
									<td>
										<button class="btn btn-info"
											onclick="getAddress(${course.id}, 'VIEW');">
											view
										</button>
										<button class="btn btn-primary"
											onclick="getAddress(${course.id}, 'EDIT');">
											edit
										</button>
										<button class="btn btn-danger"
											onclick="deleteAddress(${course.id});">
											delete
										</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center">no address</th>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th colspan="5">
							<button onclick="location.href='addAddress'"
								class="btn btn-primary">
								Add address
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>