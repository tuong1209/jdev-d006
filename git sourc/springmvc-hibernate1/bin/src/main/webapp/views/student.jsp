<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/student.js" />"></script>
<title>Insert title here</title>
</head>
<body>
	<div class="panel panel-default">
		<div id="abc" class="panel-heading h3 text-center">student</div>
		<div class="panel-body"></div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>id</th>
					<th>name</th>
					<th>age</th>
					<th>address</th>
					<th>action</th>
				</tr>
			</thead>
			<c:choose>
				<c:when test="${!empty students }">
					<c:forEach items="${students }" var="student">
						<tr>
							<td>${student.id }</td>
							<td>${student.firstname }</td>
							<td>${student.lastname }</td>
							<td>${student.age }</td>
							<td>${student.address.strert },${student.address.disstrict },${student.address.city }</td>
							<td>
								<button class="btn btn-info" onclick="getStudent(${student.id}, 'VIEW');">view</button>
							
								<button class="btn btn-primary"	onclick="getStudent(${student.id}, 'EDIT');">edit</button>
										
								<button class="btn btn-danger" onclick="deleteStudent(${student.id});">delete</button>
					</c:forEach>
				</c:when>
				<c:otherwise>
							<tr>
								<th colspan="5" class="text-center">no student</th>
							</tr>
				</c:otherwise>
			</c:choose>
			<tr>
						<th colspan="5">
							<button onclick="location.href='addStudent'" class="btn btn-primary">
								add
							</button>
						</th>
			</tr>	
		</table>
	</div>
</body>
</html>