function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addStudent() {
	location.href='addStudent';
}

function getStudent(id, mode) {
	location.href='student?id=' + id + "&mode=" + mode;
};

function getStudent() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "controller/students";
};

function deleteStudent(id) {
	var result = confirm("Do you want to delete this student ?");
	if (result == true) {
		sendDeleteRequest('./student/' + id, function(){location.href='./student';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}