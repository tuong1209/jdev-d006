package com.website.springmvc.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.website.springmvc.entity.Student;

@Repository 
public class StudentDAO extends EntityDAO<Student> {

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Student> getAll() {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		List<Student> students =session.createQuery("from Student").list();
		return students;
	}

	@Override
	public Student get(long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		
		return (Student)session.get(Student.class, new Long(id));
	}

	@Override
	public Student insert(Student t) {
		Session session=this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean update(Student t) {
		Session session=this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			// TODO: handle exception
			return Boolean.FALSE;
		}
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Student t) {
		Session session=this.sessionFactory.getCurrentSession();
		if (null!=t) {
			session.delete(t);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(long id)  {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		Student student=(Student)session.load(Student.class, new Long(id));
		if (null!=student) {
			session.delete(student);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	
}
