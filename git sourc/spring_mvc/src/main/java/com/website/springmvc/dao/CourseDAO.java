package com.website.springmvc.dao;

import java.util.Collections;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Course;
import com.website.springmvc.entity.Student;

public abstract class CourseDAO extends EntityDAO<Course> {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Course> getAll() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<Course> courses = session.createQuery("from Course").list();
		return courses;
	}

	@Override
	public Course get(long id) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();

		return (Course) session.get(Course.class, new Long(id));
	}

	@Override
	public Course insert(Course t) {
		Session session=this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
		// TODO Auto-generated method stub

	}

	@Override
	public boolean update(Course t) {
		Session session=this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			// TODO: handle exception
			return Boolean.FALSE;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public boolean delete(Course t) {
		Session session=this.sessionFactory.getCurrentSession();
		if (null!=t) {
			session.delete(t);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		// TODO Auto-generated method stub

	}

}
