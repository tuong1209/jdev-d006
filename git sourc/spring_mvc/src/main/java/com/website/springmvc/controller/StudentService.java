package com.website.springmvc.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.website.springmvc.dao.EntityDAO;
import com.website.springmvc.entity.Student;

@Transactional
@Service
public class StudentService {
	@Autowired
	EntityDAO<Student> studentDao;
	
	public List<Student> getall() {
		return studentDao.getAll();
	}
	public Student get(Long id) {
		return studentDao.get(id);
	}
	public Student add(Student student) throws Exception {
		return studentDao.insert(student);
	}
	public Boolean update(Student student) {
		return studentDao.update(student);
	}
	public Boolean delete(Student student) {
		return studentDao.delete(student);
	}
	public Boolean delete(Long id)  {
		return studentDao.delete(id);
	}
}
