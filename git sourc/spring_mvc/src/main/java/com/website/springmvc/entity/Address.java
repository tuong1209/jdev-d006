package com.website.springmvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="address", catalog="website")
public class Address implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false, unique=true)
	private long id;
	
	@Column(name="STREET", length=100)
	private String street;
	
	@Column(name="DISTRICT", length=100)
	private String district;
	
	@Column(name="CITY", length=100)
	private String city;

	public Address() {
	}
	
	public Address(String street, String district, String city) {
		this.street = street;
		this.district = district;
		this.city = city;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String toString(){
		return String.format("{id:%s,street:%s,district:%s,city:%s}", id, street, district, city);
	}
}
