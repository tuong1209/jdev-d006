package com.website.springmvc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "student", catalog = "website")
public class Student implements Serializable {

	private static final Long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;

	@Column(name = "FIRST_NAME", length = 20)
	private String firstName;

	@Column(name = "LAST_NAME", length = 20)
	private String lastName;

	@Column(name = "AGE")
	private int age;

	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="ADDRESS_ID")
	private Address address;

	@ManyToOne
	@JoinColumn(name="CLAZZ_ID", nullable=false)
	private Clazz clazz;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
			name="STUDENT_COURSE", 
			joinColumns={@JoinColumn(name="STUDENT_ID")}, 
			inverseJoinColumns={@JoinColumn(name="COURSE_ID")}
			)
	List<Course> course;
	
	public Student() {
	}

	public Student(String firstName, String lastName, int age, Address add, Clazz clazz) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.address = add;
		this.clazz = clazz;
	}

	public Student(String firstName, String lastName, Integer age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}
	
	public List<Course> getCourse() {
		return course;
	}

	public void setCourse(List<Course> course) {
		this.course = course;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public static Long getSerialversionuid() {
		return serialVersionUID;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Clazz getClazz() {
		return clazz;
	}

	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}
	
	public String toString() {
		return String.format("{id:%s,firstName:%s,lastName:%s,age:%s,address:%s}", id, firstName, lastName, age,
				address.toString());
	}
}
