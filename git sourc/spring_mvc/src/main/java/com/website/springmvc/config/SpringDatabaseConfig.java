package com.website.springmvc.config;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.website.springmvc.entity.Address;
import com.website.springmvc.entity.Clazz;
import com.website.springmvc.entity.Course;
import com.website.springmvc.entity.Student;



@EnableTransactionManagement
@Configuration

public class SpringDatabaseConfig extends WebMvcConfigurerAdapter{

	@Bean
	public LocalSessionFactoryBean sessionFactoryBean(BasicDataSource dataSource)
	{
		LocalSessionFactoryBean sessionFactoryBean=new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean.setPackagesToScan(new String[]{"com.website.springmvc.entity"});
		sessionFactoryBean.setAnnotatedClasses(Student.class,Address.class,Course.class,Clazz.class);
		sessionFactoryBean.setHibernateProperties(hibernateProperties());
		return sessionFactoryBean;
	}

	private Properties hibernateProperties() {
		Properties properties =new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.format_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}
	
	@Bean(name="dataSource")
	public BasicDataSource getDataSource()
	{
		BasicDataSource dataSource=new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jbdc:mysql://localhost:3306/website");
		dataSource.setUsername("root");
		dataSource.setPassword("admin");
		return dataSource;
	}
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s)
	{
		HibernateTransactionManager txManager =new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}
