package com.website.springmvc.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Address;
import com.website.springmvc.entity.Clazz;



public class ClazzDAO extends EntityDAO<Clazz>{

	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<Clazz> getAll() {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		List<Clazz> clazzs =session.createQuery("from Clazz").list();
		return clazzs;
	}

	@Override
	public Clazz get(long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		
		return (Clazz)session.get(Clazz.class, new Long(id));
	}

	@Override
	public Clazz insert(Clazz t) {
		Session session=this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean update(Clazz t) {
		Session session=this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			// TODO: handle exception
			return Boolean.FALSE;
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Clazz t){
		Session session=this.sessionFactory.getCurrentSession();
		if (null!=t) {
			session.delete(t);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		Address student=(Address)session.load(Address.class, new Long(id));
		if (null!=student) {
			session.delete(student);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	
}
