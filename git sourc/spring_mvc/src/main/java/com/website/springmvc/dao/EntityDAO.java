package com.website.springmvc.dao;

import java.util.List;

public abstract class EntityDAO<T> {
	
	public abstract List<T> getAll();	
	public abstract T get(long id);
	public abstract T insert(T t) throws Exception;
	public abstract boolean update(T t) ;
	public abstract boolean delete(T t);
	public abstract boolean delete(long id);
}
