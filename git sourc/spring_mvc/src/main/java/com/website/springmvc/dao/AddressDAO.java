package com.website.springmvc.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.website.springmvc.entity.Address;




public class AddressDAO extends EntityDAO<Address> {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Address> getAll() {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		List<Address> students =session.createQuery("from Address").list();
		return null;
	}

	@Override
	public Address get(long id) {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		
		return (Address)session.get(Address.class, new Long(id));
	}

	@Override
	public Address insert(Address t) {
		Session session=this.sessionFactory.getCurrentSession();
		session.save(t);
		return t;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean update(Address t) {
		Session session=this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			// TODO: handle exception
			return Boolean.FALSE;
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Address t) {
		Session session=this.sessionFactory.getCurrentSession();
		if (null!=t) {
			session.delete(t);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(long id)  {
		// TODO Auto-generated method stub
		Session session=this.sessionFactory.getCurrentSession();
		Address address=(Address)session.load(Address.class, new Long(id));
		if (null!=address) {
			session.delete(address);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	

}
