<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container" style="margin:20px">
		<div class="row">
			<ul class="nav nav-pills">
				<h3>Admin</h3>
				<li class="active"><a href="#home" data-toggle="tab">Home</a></li>
				<li><a href="#user" data-toggle="tab">User Management</a></li>
				<li><a href="#customer" data-toggle="tab">Customer</a></li>
				<li><a href="#book" data-toggle="tab">Book Management</a></li>
				<li><a href="#order_details" data-toggle="tab">Order Management</a></li>
			</ul>
		</div>
	</div>
</body>
</html>