package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Customer;

@Repository
public class CustomerDao extends Dao<Customer>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Customer> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Customer").list();
		}
		@Override
		public Customer get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Customer) session.get(Customer.class, new Long(id));
		}
		@Override
		public Customer add(Customer customer) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(customer);
			return customer;
		}
		@Override
		public Boolean update(Customer customer) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(customer);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Customer Customer) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != Customer) {
				try {
					session.delete(Customer);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Customer customer = (Customer) session.load(Customer.class, new Long(id));
			if (null != customer) {
				session.delete(customer);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
