package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Order;

@Repository
public class OrderDao extends Dao<Order>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Order> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Order").list();
		}
		@Override
		public Order get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Order) session.get(Order.class, new Long(id));
		}
		@Override
		public Order add(Order order) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(order);
			return order;
		}
		@Override
		public Boolean update(Order order) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(order);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Order order) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != order) {
				try {
					session.delete(order);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Order order = (Order) session.load(Order.class, new Long(id));
			if (null != order) {
				session.delete(order);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
