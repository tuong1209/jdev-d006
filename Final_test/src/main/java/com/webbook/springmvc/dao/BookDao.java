package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Book;

@Repository
public class BookDao extends Dao<Book>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Book> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Book").list();
		}
		@Override
		public Book get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Book) session.get(Book.class, new Long(id));
		}
		@Override
		public Book add(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(book);
			return book;
		}
		@Override
		public Boolean update(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(book);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Book book) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != book) {
				try {
					session.delete(book);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Book book = (Book) session.load(Book.class, new Long(id));
			if (null != book) {
				session.delete(book);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
