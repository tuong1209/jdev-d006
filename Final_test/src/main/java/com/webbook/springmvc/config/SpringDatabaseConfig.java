package com.webbook.springmvc.config;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.webbook.springmvc.entities.Address;
import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Customer;
import com.webbook.springmvc.entities.Genre;
import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.Order_details;
import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.entities.User;

@EnableTransactionManagement
@Configuration
public class SpringDatabaseConfig extends WebMvcConfigurerAdapter {
	@Bean(name="sessionFactory")
	public LocalSessionFactoryBean sessionFactoryBean(BasicDataSource dataSource)
	{
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		sessionFactory.setPackagesToScan(new String[] { "com.webbook.springmvc.entities" });
		sessionFactory.setAnnotatedClasses(User.class, Address.class, Author.class, Book.class, Customer.class, Genre.class, Order.class, Order_details.class, Publisher.class);
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}
	
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.format_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}

	@Bean(name = "dataSource")
	public BasicDataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/webbook");
		dataSource.setUsername("root");
		dataSource.setPassword("");
		dataSource.setInitialSize(10);
		return dataSource;
	}

	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}