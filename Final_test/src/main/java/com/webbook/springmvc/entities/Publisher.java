package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "publisher" , catalog = "webbook")
public class Publisher {
	private static final long serialVersionUID = 1L;

	private Long id_publisher;
	private String name_publisher;
	private Address address;
	
	public Publisher() {
	}
	
	public Publisher(String name_publisher) {
		this.name_publisher = name_publisher;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_PUBLISHER", unique = true, nullable = false)
	public Long getId_publisher() {
		return id_publisher;
	}
	public void setId_publisher(Long id_publisher) {
		this.id_publisher = id_publisher;
	}
	
	@Column(name = "NAME_PUBLISHER" , length = 50)
	public String getName_publisher() {
		return name_publisher;
	}
	public void setName_publisher(String name_publisher) {
		this.name_publisher = name_publisher;
	}
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
