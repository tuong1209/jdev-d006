package com.webbook.springmvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.PublisherService;



@Controller
@RequestMapping(value="/bookcontroller")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private PublisherService publisherService;
	
	@RequestMapping(value="/books",method=RequestMethod.GET)
	public ModelAndView getAll()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookList");
		model.addObject("books", bookService.getAll());
		return model;
	}
	
	@RequestMapping(value="/addbook",method=RequestMethod.GET)
	public ModelAndView addBook() 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", new Book());
		model.addObject("publisher", publisherService.getAll());
		model.addObject("mode", "EDIT");
		return model;
	}
	
	@RequestMapping(value="/book", method=RequestMethod.POST)
	public String saveBook(@ModelAttribute("book")Book book,@RequestParam(name="publisher")String publisher) 
	{
		List<Publisher> list = new ArrayList<Publisher>();
		String[] ids = publisher.split(",");
		for (int i = 0; i < ids.length; i++) {
			list.add(publisherService.get((Long.parseLong(ids[i]))));
		}
		book.setPublisher(list);
		if(book.getId_book()==null)
		{
			bookService.add(book);
		}
		else
		{
			bookService.update(book);
		}
		return "redirect:/usercontroller/books";
	}
	@RequestMapping(value="/book",method=RequestMethod.GET)
	public ModelAndView getBookbyId(@RequestParam(name="ID_BOOK") Long id,@RequestParam(name="mode")String mode) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", bookService.get(id));
		model.addObject("publishers", publisherService.getAll());
		model.addObject("mode", mode);
		return model;
	}
	@RequestMapping(value = "/book/{ID_BOOK}", method = RequestMethod.DELETE)
	public @ResponseBody void btnDelete(@PathVariable(value = "ID_BOOK") Long id) {
		bookService.delete(id);
	}
}
