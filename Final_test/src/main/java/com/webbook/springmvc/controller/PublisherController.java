package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.PublisherService;


@Controller
@RequestMapping(value="/publishercontroller")
public class PublisherController {
	@Autowired
	private PublisherService publisherService;
	
	@RequestMapping(value = "/publishers", method = RequestMethod.GET)
	public ModelAndView getAllPublisher() {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherlist");
		model.addObject("publisherlist", publisherService.getAll());

		return model;
	}

	@RequestMapping(value = "/publisher", method = RequestMethod.GET)
	public ModelAndView getPublisherById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherDetail");
		model.addObject("mode", mode);
		model.addObject("publisher", publisherService.get(id));

		return model;
	}

	@RequestMapping(value = "/publisher", method = RequestMethod.POST)
	public String savePublisher(@ModelAttribute("publisher") Publisher publisher) {
		if (publisher.getId_publisher() == null) {
			publisherService.add(publisher);
		} else {
			publisherService.update(publisher);
		}

		return "redirect:/publishercontroller/publishers";
	}

	@RequestMapping(value = "/addPublisher", method = RequestMethod.GET)
	public ModelAndView addPublisher() {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherDetail");
		model.addObject("publisher", new Publisher());
		model.addObject("mode", "EDIT");

		return model;
	}
	
	@RequestMapping(value = "/publisher/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id) {		
		publisherService.delete(id);
	}
}
