package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.service.AuthorService;


@Controller
@RequestMapping(value="/authorcontroller")
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ModelAndView getAllAuthor() {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorList");
		model.addObject("authorlist", authorService.getAll());

		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ModelAndView getAuthorById(@RequestParam(name = "ID_AUTHOR") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("mode", mode);
		model.addObject("author", authorService.get(id));

		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public String saveAuthor(@ModelAttribute("author") Author author) {
		if (author.getId() == null) {
			authorService.add(author);
		} else {
			authorService.update(author);
		}

		return "redirect:/authorcontroller/authors";
	}

	@RequestMapping(value = "/addauthor", method = RequestMethod.GET)
	public ModelAndView addAuthor() {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("author", new Author());
		model.addObject("mode", "EDIT");

		return model;
	}
	
	@RequestMapping(value = "/author/{ID_AUTHOR}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("ID_AUTHOR") Long id) {		
		authorService.delete(id);
	}
}
