package com.webbook.springmvc.controller;

import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/user")
public class AddUserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/adduser",method=RequestMethod.POST)
	public String AddUser(String username) {
		if (userService.getuserbyname(username)==null) {
			System.out.println("insert thanh cong");
			return "true";
		}
		else {
			System.out.println("insert that bai");
		}
		return "false";
	}
	
	
}
