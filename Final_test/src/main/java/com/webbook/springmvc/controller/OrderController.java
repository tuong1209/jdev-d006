package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.OrderService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/ordercontroller")
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ModelAndView getAllOrder() {
		ModelAndView model = new ModelAndView();

		model.setViewName("orderList");
		model.addObject("orders", orderService.getAll());

		return model;
	}
	
	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public ModelAndView getOrderById(@RequestParam(name = "ID_ORDER") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("mode", mode);
		model.addObject("order", orderService.get(id));

		return model;
	}
	
	@RequestMapping(value="/addorder",method=RequestMethod.GET)
	public ModelAndView addOther() 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("orderList");
		model.addObject("order", new Order());
		model.addObject("users", userService.getAll());
		return model;
	}
	
	@RequestMapping(value="/order",method=RequestMethod.POST)
	public String saveOrder(@ModelAttribute("order")Order order,@RequestParam(name="user")String Suser) 
	{
		User user=userService.get(Long.parseLong(Suser));
		order.setUser(user);
		if (order.getId_order()==null) 
		{
			orderService.add(order);
		}
		else {
			orderService.update(order);
		}
		return "redirect:/ordercontroller/orders";
	}
	@RequestMapping(value = "/order/{ID_ORDER}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("ID_ORDER") Long id) {		
		orderService.delete(id);
	}
}
