package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.webbook.springmvc.entities.Customer;
import com.webbook.springmvc.service.CustomerService;

@Controller
@RequestMapping(value="/customercontroller")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public ModelAndView getAllCustomer() {
		ModelAndView model = new ModelAndView();

		model.setViewName("customerList");
		model.addObject("customers", customerService.getAll());

		return model;
	}

	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public ModelAndView getCustomerById(@RequestParam(name = "ID_CUSTOMER") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("customerDetail");
		model.addObject("mode", mode);
		model.addObject("customer", customerService.get(id));

		return model;
	}

	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {
		if (customer.getId_customer() == null) {
			customerService.add(customer);
		} else {
			customerService.update(customer);
		}

		return "redirect:/customercontroller/customers";
	}

	@RequestMapping(value = "/addcustomer", method = RequestMethod.GET)
	public ModelAndView addCustomer() {
		ModelAndView model = new ModelAndView();

		model.setViewName("customerDetail");
		model.addObject("customer", new Customer());
		model.addObject("mode", "EDIT");

		return model;
	}
	
	@RequestMapping(value = "/customer/{ID_CUSTOMER}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("ID_CUSTOMER") Long id) {		
		customerService.delete(id);
	}
}
