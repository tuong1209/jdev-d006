function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		data :JSON.stringify(data),
		type : method,
		contentType : "application/json",
		success : callback,
		error : function(request, msg, error) {
		}
	});
};

function getBook(id, mode) {
	location.href = "./book?id=" + id + "&mode=" + mode;
};

function deleteBook(id, row) {
	var result = confirm("Do you want to delete this book ?")
	if (result){
		deleteBookOnTable(id, row);
		sendDeleteRequest("./book/" + id, function() {
			location.href = "./books"
		});
	}
};

function deleteBookOnTable(id, row){
	var table = document.getElementById("bookHome");
	var tbody = table.getElementsByTagName("tbody");
	var tr = tbody[0].getElementsByTagName("tr");
	tbody[0].deleteRow(tr[row.rowIndex-1]);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", "DELETE", callback);
}


function getId() {
	var strId = "";
	var table = document.getElementById("tbBooks");
	var tbody = table.getElementsByTagName("tbody");
	var tr = tbody[0].getElementsByTagName("tr");
	var td
	for (var i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td");
		if (i != tr.length - 1)
			strId += (td[0].innerHTML + ",");
		else
			strId += td[0].innerHTML;

	}
	var btn = document.getElementById("saveBtn");
	btn.value = strId;
};

function addToTable(id, cname) {
	var booksRegistered = document.getElementById("tbBooks");
	var tbody = booksRegistered.getElementsByTagName("tbody");
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	var text = document.createTextNode(id);
	td.appendChild(text);
	tr.appendChild(td);
	tbody[0].appendChild(tr);
	td = document.createElement("td");
	text = document.createTextNode(cname);
	td.appendChild(text);
	tr.appendChild(td);
	tbody[0].appendChild(tr);
};

function exist(id) {
	var booksRegistered = document.getElementById("tbBooks");
	var tbody = booksRegistered.getElementsByTagName("tbody");
	var tr = tbody[0].getElementsByTagName("tr");
	var td;
	for (var i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td");
		if (td[0].innerHTML == id)
			return true;
	}
	return false;
};

function deleteARow(id) {
	var booksRegistered = document.getElementById("tbBooks");
	var tbody = booksRegistered.getElementsByTagName("tbody");
	var tr = tbody[0].getElementsByTagName("tr");
	var td;
	for (var i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td");
		if (td[0].innerHTML == id)
			tbody[0].deleteRow(i);
	}
};

function bookClicked(x) {
	var listBooks = document.getElementById("listBooks");
	if (x.style.backgroundColor == "yellow") {
		x.style.backgroundColor = "";
		var listCourses = document.getElementById("listBooks");
		var tr = listCourses.getElementsByTagName("tr");
		var td = tr[x.rowIndex].getElementsByTagName("td");
		deleteARow(td[0].innerHTML);
	} else {
		var tr = listBooks.getElementsByTagName("tr");
		var td = tr[x.rowIndex].getElementsByTagName("td");
		if (exist(td[0].innerHTML))
			alert("This book is registered!!!");
		else {
			var listBooks = document.getElementById("listBooks");
			x.style.backgroundColor = "yellow";
			addToTable(td[0].innerHTML, td[1].innerHTML);
		} 
	}
};

function bookClickedForEdit(x) {
	var result = confirm("Do you want to delete this book?");
	if (result) {
		var booksRegistered = document.getElementById("tbBooks");
		var tbody = booksRegistered.getElementsByTagName("tbody");
		tbody[0].deleteRow(x.rowIndex - 1);
	}
};