function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addPublisher() {
	location.href='addPublisher';
}

function getPublisher(id, mode) {
	location.href='publisher?id=' + id + "&mode=" + mode;
};

function getPublishers() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "publishercontroller/publishers";
};

function deletePublisher(id) {
	var result = confirm("Do you want to delete this publisher ?");
	if (result == true) {
		sendDeleteRequest('./publisher/' + id, function(){location.href='./publishers';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}