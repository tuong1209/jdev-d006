function sendRequest(url, data, method, callback) {
	$.ajax({
		url : url,
		type : method,
		data : data,
		contentType : 'application/json',
		success : callback,
		error : function(request, msg, error) {
			// handle failure
		}
	});
};

function sendGetRequest(url, callback) {
	sendRequest(url, "", 'GET', callback);
};

function sendPostRequest(url, data, callback) {
	sendRequest(url, data, 'POST', callback);
};

function sendPutRequest(url, data, callback) {
	sendRequest(url, data, 'PUT', callback);
};

function sendDeleteRequest(url, callback) {
	sendRequest(url, "", 'DELETE', callback);
};

function addUser() {
	location.href='addUser';
}

function getUser(id_user, mode) {
	location.href='user?id_user=' + id_user + "&mode=" + mode;
};

function getUsers() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "usercontroller/users";
};

function deleteUser(id_user) {
	var result = confirm("Do you want to delete this user ?");
	if (result == true) {
		sendDeleteRequest('./user/' + id_user, function(){location.href='./users';});
	}	
};

function getContextRootPath() {
	
	 var pathName = location.pathname.substring(1);
	 var token = pathName.split("/");	 
	 var rootContextPath = "/" + token[0] + "/";
	 
	 return rootContextPath;
}