<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/books.js" />"></script>
<title><spring:message code="book.detail.label"></spring:message></title>

<style type="text/css">
table tr, th, td {
			text-align: center;
}

table th {
	font-size: 20px;
}

table td {
	font-size: 15px;
}

</style>


<title>More Detail</title>

</head>
<body>
<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="#">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li class="active"><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li><a href="adminuser">User</a></li>
			          <li><a href="adminpublisher">Publisher</a></li>
			          <li><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="#">message</a></li>
		    </ul>
	   </div>
	</div>
	<div class="panel panel-defualt">
		<div class="panel-heding h3 text-center">
			<div class="col-md-6">Book Detail</div>
			<br>
			<div class="panel-body">
				<form:form action="./book" class="form-horizontal" method="post" modelAttribute="book">
					<form:input path="id" type="hidden" />
					<div class="col-md-6">
						<div class="form-group">
							<label for="txtName" class="control-lable col-md-4">Name</label>
							<div class="col-md-6">
								<form:input type="text" path="name" class="form-control"
									id="txtName" placeholder="Name"
									readonly="${mode=='VIEW'}" />
							</div>
						</div>

						<div class="form-group">
							<label for="txtPrice" class="control-lable col-md-4">Price</label>
							<div class="col-md-6">
								<form:input type="text" path="price" class="form-control"
									id="txtPrice" placeholder="Price"
									readonly="${mode=='VIEW'}" />
							</div>
						</div>	
						<c:if test="${mode=='EDIT' }">
<!-- 						author -->
							<div class="form-group">
								<label for="txt-author" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.author"/>
								</label>
								
									<c:choose>
										<c:when test="${!empty authorlist }">
										<form:select path="author_id.id" id="author" readonly="${mode=='VIEW'}">
			      							<form:options items="${authorlist}" itemValue="id" itemLabel="name"/>
										</form:select>
										</c:when>	
										<c:otherwise>
											<spring:message code="book.detail.noauthor"/>
										</c:otherwise>
									</c:choose>
								
							</div>
							
	<!--  						Genre  -->
							<div class="form-group">
								<label for="txt-genre" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.genre"/>
								</label>
								
								<c:choose>
									<c:when test="${!empty genres }">
										<form:select path="genre_id.id" readonly="${mode=='VIEW'}">
											<form:options items="${genres }" itemValue="id" itemLabel="description"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<spring:message code="book.detail.nogenre"/>
									</c:otherwise>
								</c:choose>
								
							</div>
							
	<!-- 						publisher -->
							<div class="form-group">
								<label for="txt-publisher" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.publisher"/>
								</label>
								<c:choose>
									<c:when test="${!empty publishers }">
										<form:select path="publisher.id">
											<form:options items="${publishers}" itemValue="id" itemLabel="publisher_name"/>
										</form:select>
									</c:when> 
	 								<c:otherwise> 
	 									<spring:message code="book.detail.nopublisher"/> 
									</c:otherwise> 
								</c:choose> 
							</div>

						</c:if>
						
						<c:if test="${mode=='VIEW' }">
<!-- 						author -->
							<div class="form-group">
								<label	for="txt-author" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.author"/>
								</label>
								<div class="col-md-6">
									<form:input path="author_id.name" type="text" readonly="${mode=='VIEW' }" class="form-control" />
								</div>
							</div>
<!-- 							genre -->
							<div class="form-group">
								<label	for="txt-genre" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.genre"/>
								</label>
								<div class="col-md-6">
									<form:input path="genre_id.description" type="text" readonly="${mode=='VIEW' }" class="form-control" />
								</div>
							</div>
							
							
<!-- 							publisher -->	
							<div class="form-group">
								<label for="txt-publisher" class="control-lable col-md-4">
									<spring:message code="book.detail.lab.publisher"/>
								</label>
								<div class="col-md-6">
									<form:input path="publisher.publisher_name" type="text" readonly="${mode=='VIEW' }" class="form-control" />
								</div>
							</div>
						</c:if>




						<div class="btn-group" role="group" aria-label="Basic example">
							<c:if test="${mode == 'EDIT' ||  mode == 'ADD'}">
								<button type="submit" class="btn btn-primary" name="btnSave"
									id="saveBtn">Save</button>
							</c:if>

							<button type="button" class="btn btn-danger"
								onclick="location.href='./books'">Cancel</button>
							
						</div>
					</div>

					
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>