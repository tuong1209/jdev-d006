<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/users.js" />"></script>
	
<style type="text/css">
table tr, th, td {
	text-align: center;
}
</style>
<title><spring:message code="user.label" /></title>
</head>
<body>
<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="#">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li class="active"><a href="adminuser">User</a></li>
			          <li><a href="adminpublisher">Publisher</a></li>
			          <li><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="#">message</a></li>
		    </ul>
	   </div>
	</div>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="user.header"></spring:message>
		</div>
		<div class="panel-body">
			<table class="table table-striped" id="userHome">
				<thead>
					<tr>
						<th><spring:message code="user.table.id" /></th>
						<th><spring:message code="user.table.name" /></th>
						<th><spring:message code="user.table.phones" /></th>
						<th><spring:message code="user.table.action" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty users}">
							<c:forEach items="${users}" var="user">
								<tr>
									<td>${user.id_user}</td>
									<td>${user.name_user}</td>
									<td>${user.phones}</td>
									<td>
										<button class="btn btn-default"
											onclick="getUser(${user.id_user}, 'VIEW');">
											<spring:message code="user.btn.view" />
										</button>
										<button class="btn btn-primary"
											onclick="getUser(${user.id_user}, 'EDIT');">
											<spring:message code="user.btn.edit" />
										</button>
										<button class="btn btn-danger"
											onclick="deleteUser(${user.id_user}, this);">
											<spring:message code="user.btn.delete" />
										</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center"><spring:message
										code="user.table.empty" /></th>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th colspan="5">
							<button onclick="addUser();"
								class="btn btn-primary">
								<spring:message code="user.btn.add" />
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>