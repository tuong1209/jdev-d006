<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<title>Insert title here</title>
</head>
<body>
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="./getauthor" method="get">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li class="active"><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li><a href="adminuser">User</a></li>
			          <li><a href="adminpublisher">Publisher</a></li>
			          <li><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="customer">message</a></li>
		    </ul>
	   </div>
	</div>
	<div class="container"> 
		<div class="row">
			<div class="col-sm-4 well">
				Doanh thu: ${price } 
			</div>
			<div class="col-sm-4 well">
				don hang: ${sizeorder } 
			</div>
			<div class="col-sm-4 well">
				tin nhan: ${sizemessages }
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 well">
				<div>
					<strong>Doanh thu theo thang:</strong> ${price }
				</div>
				<div>
					<form action="#">
						<input type="text" placeholder="date" id="date1">
						<input type="text" placeholder="date" id="date2">
						<input type="submit" value="date">
					</form>
				</div>
			</div>
			<div class="col-sm-6 well">
				<div style="text-align: center;">
					<strong>Message</strong>
				</div>
				<c:choose>
					<c:when test="${!empty messages }">
						<c:forEach items="${messages }" var="message">
							<div class="alert alert-info">
								${message.name_customer } : ${message.message }
							</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						No message 
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	
</body>
</html>