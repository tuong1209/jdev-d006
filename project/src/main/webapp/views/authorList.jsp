<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/author.js" />"></script>

<title><spring:message code="author.label" /></title>
</head>
<body>
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="#">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li><a href="adminuser">User</a></li>
			          <li><a href="adminpublisher">Publisher</a></li>
			          <li class="active"><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="#">message</a></li>
		    </ul>
	   </div>
	</div>
	
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="author.header"/>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><spring:message code="author.table.id"/></th>
						<th><spring:message code="author.table.name"/></th>
						<th><spring:message code="author.table.action"/></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty authors}">
							<c:forEach var="author" items="${authors}" >
								<tr>
									<td>${author.id}</td>
									<td>${author.name}</td>
									<td>
										<button class="btn btn-info" onclick=" getAuthor(${author.id}, 'VIEW');">
											<spring:message code="author.btn.view"/>
										</button>
										<button class="btn btn-primary" onclick=" getAuthor(${author.id }, 'EDIT');">
											<spring:message code="author.btn.edit"/>
										</button>
										<button class="btn btn-danger" onclick="deleteAuthor(${author.id });">
											<spring:message code="author.btn.delete"/>
										</button>
									</td>
								</tr>
							</c:forEach>
							</c:when> 
	 						<c:otherwise> 
 							<tr> 
 								<th colspan="5" class="text-center">
 								<spring:message code="author.table.empty"/> 
 								</th> 
 							</tr> 
 						</c:otherwise>
 					</c:choose> 
					<tr>
						<th colspan="5">
							<button class="btn btn-primary" onclick="addAuthor();">
								<spring:message code="author.btn.add"/>
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>