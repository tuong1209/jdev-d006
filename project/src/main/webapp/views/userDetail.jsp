<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/users.js" />"></script>
<title><spring:message code="user.detail.label"></spring:message></title>

<style type="text/css">
table tr, th, td {
	text-align: center;
}

table th {
	font-size: 20px;
}

table td {
	font-size: 15px;
}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$("tr.rows").click(function() {
			alert("Click!");
		});
	});
</script>
<title>More Detail</title>

</head>
<body>
<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="#">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li class="active"><a href="adminuser">User</a></li>
			          <li><a href="adminpublisher">Publisher</a></li>
			          <li><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="#">message</a></li>
		    </ul>
	   </div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading h3 text-center"><spring:message code="user.detail.label" /></div>
		<div class="panel-body">
			<form:form class="form-horizontal" action="./user" method="post" modelAttribute="user">
				<form:input path="id_user" type="hidden"/>
<!-- 				name -->
				<div class="form-group">
					<label class="control-label col-sm-4" for="NameUser">
						<spring:message code="user.detail.name" />
					</label>
					<div class="col-sm-6">
						<form:input path="name_user" type="text" class="form-control" id="NameUser" 
								name="NameUser" placeholder="User name" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
<!-- 				phone -->
				<div class="form-group">
					<label class="control-label col-sm-4" for="phoneUser">
						<spring:message code="user.detail.phone" />
					</label>
					<div class="col-sm-6">
						<form:input path="phones" type="text" class="form-control" id="phoneUser" 
								name="phoneUser" placeholder="Phone name" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
				
<!-- 				address -->
				<form:input path="address.id" type="hidden"/>
<!-- 					street -->
				<div class="form-group">
					<label class="control-label col-sm-4" for="street">
						<spring:message code="address.street" />
					</label>
					<div class="col-sm-6">
						<form:input path="address.street" type="text" class="form-control" id="street" 
								name="street" placeholder="Street" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
<!-- 					district -->
				<div class="form-group">
					<label class="control-label col-sm-4" for="district">
						<spring:message code="address.district" />
					</label>
					<div class="col-sm-6">
						<form:input path="address.district" type="text" class="form-control" id="street" 
								name="district" placeholder="District" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
<!-- 					city -->
				<div class="form-group">
					<label class="control-label col-sm-4" for="city">
						<spring:message code="address.city" />
					</label>
					<div class="col-sm-6">
						<form:input path="address.city" type="text" class="form-control" id="city" 
								name="city" placeholder="City" readonly="${mode == 'VIEW'}"/>
					</div>
				</div>
				
				
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-6">
						<c:if test = "${mode == 'VIEW'}">
							<button disabled="disabled" type="submit" class="btn btn-primary">
								<spring:message code="user.detail.btn.save" />
							</button>
						</c:if>
						<c:if test = "${mode == 'EDIT'}">
							<button type="submit" class="btn btn-primary">
								<spring:message code="user.detail.btn.save" />
							</button>
						</c:if>
						<button type="button" onclick="getUsers();" class="btn btn-default">
							<spring:message code="user.detail.btn.cancel" />
						</button>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>