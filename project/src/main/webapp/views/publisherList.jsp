<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
			pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/publishers.js" />"></script>
<title><spring:message code="publisher.label" /></title>
</head>
<body>
<div class="navbar navbar-default">
		<div class="container-fluid">
			<a href="#" class="navbar-brand">
				logo web
			</a>
			<form class="navbar-form navbar-left" action="#">
		      <div class="input-group">
		        <input type="text" class="form-control" placeholder="Search" name="search">
		        <div class="input-group-btn">
			        <select name="name_search" class="form-control">
							<option value="book">book</option>
							<option value="publisher">publisher</option>
							<option value="genrce">genrce</option>
							<option value="author">author</option>
							<option value="user">user</option>
					</select>
				</div>
		        <div class="input-group-btn">
		          <button class="btn btn-default" type="submit">
		           search
		          </button>
		        </div>
		      </div>
		    </form>
		    <ul class="nav navbar-nav navbar-right">
		    	<li><a href="homeadmin">home</a></li>
		    	<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">Product<span class="caret"></span></a>
			        <ul class="dropdown-menu">
			          <li><a href="adminbook">Book</a></li>
			          <li><a href="adminuser">User</a></li>
			          <li class="active"><a href="adminpublisher">Publisher</a></li>
			          <li><a href="adminauthor">Author</a></li>
			          <li><a href="admingenre">Genre</a></li>
			        </ul>
			     </li>
		    	<li><a href="#">message</a></li>
		    </ul>
	   </div>
	</div>
	<div class="panel panel-default">
		<div id="title" class="panel-heading h3 text-center">
			<spring:message code="publisher.header"></spring:message>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><spring:message code="publisher.table.id" /></th>
						<th><spring:message code="publisher.table.publisher_name" /></th>
						<th><spring:message code="publisher.table.action" /></th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${!empty publishers}">
							<c:forEach items="${publishers}" var="publisher">
								<tr>
									<td>${publisher.id}</td>
									<td>${publisher.publisher_name}</td>
									<td>
										<button class="btn btn-info"
											onclick="getPublisher(${publisher.id}, 'VIEW');">
											<spring:message code="publisher.btn.view" />
										</button>
										<button class="btn btn-primary"
											onclick="getPublisher(${publisher.id}, 'EDIT');">
											<spring:message code="publisher.btn.edit" />
										</button>
										<button class="btn btn-danger"
											onclick="deletePublisher(${publisher.id});">
											<spring:message code="publisher.btn.delete" />
										</button>
									</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<th colspan="5" class="text-center"><spring:message
										code="publisher.table.empty" /></th>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th colspan="5">
							<button onclick="location.href='addPublisher'"
								class="btn btn-primary">
								<spring:message code="publisher.btn.add" />
							</button>
						</th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>