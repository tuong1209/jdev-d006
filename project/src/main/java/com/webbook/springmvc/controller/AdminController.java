package com.webbook.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.OrderDetails;
import com.webbook.springmvc.service.AuthorService;
import com.webbook.springmvc.service.CustomerService;
import com.webbook.springmvc.service.OrderDetailService;
import com.webbook.springmvc.service.OrderService;

@Controller
@RequestMapping(value="/admin")
public class AdminController {
	
	@Autowired
	private CustomerService customerService;
	@Autowired
	private OrderDetailService orderDetailService;
	@Autowired 
	private OrderService orderService;
	@Autowired
	private AuthorService authorService;
	
	@RequestMapping(value="/view")
	public ModelAndView ViewAdmin() 
	{
		ModelAndView model =new ModelAndView();
		model.setViewName("AdminPage");
		model.addObject("messages", customerService.getAll());
		model.addObject("price", GetPrice());
		model.addObject("sizeorder", orderService.getAll().size());
		model.addObject("sizemessages", customerService.getAll().size());
		return model;
	}
	
	@RequestMapping(value="getauthor")
	public ModelAndView GetByNameAuthor(@RequestParam(value="search")String search,@RequestParam(value="name_search")String name_search)
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("authorList");
		model.addObject("authors", authorService.getAuthorByName(name_search));
		System.out.println(search+" va "+name_search);
		return model;
		
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
	@RequestMapping(value="/customer")
	public String GetCustomer()
	{
		return "redirect:/customercontroller/customers";
	}
	
	private int GetPrice() {
		List<OrderDetails>list=orderDetailService.getAll();
		int price=0;
		for (OrderDetails od : list) {
			price+=od.total_price;
		}
		return price;
	}
}
