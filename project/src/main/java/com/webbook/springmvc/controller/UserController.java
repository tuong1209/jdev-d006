package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/usercontroller")
public class UserController {

	@Autowired
	private UserService userService;

	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView getAllUser() {
		ModelAndView model = new ModelAndView();

		model.setViewName("userList");
		model.addObject("users", userService.getAll());
		System.out.println(userService.getAll());
		return model;
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ModelAndView getUserById(@RequestParam(name = "id_user") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("userDetail");
		model.addObject("mode", mode);
		model.addObject("user", userService.get(id));

		return model;
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("user") User user) {
		if (user.getId_user() == null) {
			userService.add(user);
		} else {
			userService.update(user);
		}

		return "redirect:/usercontroller/users";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public ModelAndView addUser() {
		ModelAndView model = new ModelAndView();

		model.setViewName("userDetail");
		model.addObject("user", new User());
		model.addObject("mode", "EDIT");

		return model;
	}
	
	@RequestMapping(value = "/user/{id_user}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id_user") Long id) {		
		userService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
