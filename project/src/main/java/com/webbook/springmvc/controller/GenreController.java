package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Genre;
import com.webbook.springmvc.service.GenreService;

@Controller
@RequestMapping(value="/genrecontroller")
public class GenreController {

	@Autowired
	private GenreService genreService;
	
	@RequestMapping(value="genres", method =RequestMethod.GET)
	public ModelAndView getAllGenre()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("genreList");
		model.addObject("genres", genreService.getAll());
		return model;
	}
	
	@RequestMapping(value="addGenre",method=RequestMethod.GET)
	public ModelAndView addGenre()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("genreDetail");
		model.addObject("genre", new Genre());
		model.addObject("mode", "EDIT");
		return model;
	}
	@RequestMapping(value = "/genre", method = RequestMethod.GET)
	public ModelAndView getGenreById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("genreDetail");
		model.addObject("mode", mode);
		model.addObject("genre", genreService.get(id));

		return model;
	}

	@RequestMapping(value = "/genre", method = RequestMethod.POST)
	public String saveGenre(@ModelAttribute("genre") Genre genre) {
		if (genre.getId() == null) {
			genreService.add(genre);
		} else {
			genreService.update(genre);
		}

		return "redirect:/genrecontroller/genres";
	}
	
	@RequestMapping(value = "/genre/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id) {		
		genreService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
