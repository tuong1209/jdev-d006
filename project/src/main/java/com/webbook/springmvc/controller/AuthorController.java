package com.webbook.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Author;
import com.webbook.springmvc.service.AuthorService;


@Controller
@RequestMapping(value="/authorcontroller")
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	
	@RequestMapping(value = "/authors", method = RequestMethod.GET)
	public ModelAndView getAllAuthor() {
		ModelAndView model = new ModelAndView();

		List<Author> authors = authorService.getAll();
		model.setViewName("authorList");
		model.addObject("authors", authors);
		System.out.println(authors);
		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ModelAndView getAuthorById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("mode", mode);
		model.addObject("author", authorService.get(id));

		return model;
	}

	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public String saveAuthor(@ModelAttribute("author") Author author) {
		if (author.getId() == null) {
			authorService.add(author);
		} else {
			authorService.update(author);
		}

		return "redirect:/authorcontroller/authors";
	}

	@RequestMapping(value = "/addauthor", method = RequestMethod.GET)
	public ModelAndView addAuthor() {
		ModelAndView model = new ModelAndView();

		model.setViewName("authorDetail");
		model.addObject("author", new Author());
		model.addObject("mode", "EDIT");
		System.out.println(model.getModel());
		return model;
	}
	
	@RequestMapping(value = "/author/{ID_AUTHOR}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("ID_AUTHOR") Long id) {		
		authorService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
