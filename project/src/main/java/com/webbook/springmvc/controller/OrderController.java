package com.webbook.springmvc.controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Order;
import com.webbook.springmvc.entities.OrderDetails;
import com.webbook.springmvc.entities.User;
import com.webbook.springmvc.service.OrderDetailService;
import com.webbook.springmvc.service.OrderService;
import com.webbook.springmvc.service.UserService;

@Controller
@RequestMapping(value="/ordercontroller")
public class OrderController {

	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/orderS",method=RequestMethod.GET)
	public ModelAndView getAllOrder()
	{
		ModelAndView model =new ModelAndView();
		model.setViewName("");//them view name
		model.addObject("orders", orderService.getAll());
		return model;
	}
	
	@RequestMapping(value="/order",method=RequestMethod.GET)
	public ModelAndView getOrderByID(@RequestParam(name="ID") Long id,@RequestParam(name="mode")String mode) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("");//them viewname
		model.addObject("order", orderService.get(id));
		model.addObject("orderdetail", getOrderDetailsByIDOrder(id) );
		model.addObject("mode", mode);
		return model;
	}
	///trong orderdetailDao
	@Autowired 
	SessionFactory sessionFactory;
	private List<OrderDetails> getOrderDetailsByIDOrder(Long id)
	{
		Session session=this.sessionFactory.getCurrentSession();
		
		String hql="from OrderDetails A WHERE A.order_id like :keyword";
		String keyword=String.valueOf(id);
		return session.createQuery(hql).list();		
	}
	///
	
	@RequestMapping(value="/order",method=RequestMethod.POST)
	public String SaveOrder(@ModelAttribute("order")Order order,@ModelAttribute("order_details")OrderDetails orderDetails)
	{
		orderService.update(order);
		orderDetailService.update(orderDetails);
		return "redirect:/ordercontroller/orderS";
	}
	
	@RequestMapping(value="/orderdetail/{ID}")
	public @ResponseBody void deleteOrderDetail(@PathVariable("ID") Long id)
	{
		orderDetailService.delete(id);
	}
	@RequestMapping(value="/order/{ID}")
	public @ResponseBody void deleteOrder(@PathVariable("ID") Long id)
	{
		orderService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
