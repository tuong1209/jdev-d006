package com.webbook.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.PublisherService;


@Controller
@RequestMapping(value="/publishercontroller")
public class PublisherController {
	@Autowired
	private PublisherService publisherService;
	
	@RequestMapping(value = "/publishers", method = RequestMethod.GET)
	public ModelAndView getAllPublisher() {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherList");
		model.addObject("publishers", publisherService.getAll());
		
		System.out.println(model.getModel());
		
		return model;
	}

	@RequestMapping(value = "/publisher", method = RequestMethod.GET)
	public ModelAndView getPublisherById(@RequestParam(name = "id") Long id, @RequestParam(name = "mode") String mode) {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherDetail");
		model.addObject("mode", mode);
		model.addObject("publisher", publisherService.get(id));

		return model;
	}

	@RequestMapping(value = "/publisher", method = RequestMethod.POST)
	public String savePublisher(@ModelAttribute("publisher") Publisher publisher) {
		if (publisher.getId() == null) {
			publisherService.add(publisher);
		} else {
			publisherService.update(publisher);
		}

		return "redirect:/publishercontroller/publishers";
	}

	@RequestMapping(value = "/addPublisher", method = RequestMethod.GET)
	public ModelAndView addPublisher() {
		ModelAndView model = new ModelAndView();

		model.setViewName("publisherDetail");
		model.addObject("publisher", new Publisher());
		model.addObject("mode", "EDIT");
		System.out.println(model.getModel());
		return model;
	}
	
	@RequestMapping(value = "/publisher/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("id") Long id) {		
		publisherService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
