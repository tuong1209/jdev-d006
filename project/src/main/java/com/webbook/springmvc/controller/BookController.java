package com.webbook.springmvc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.entities.Book;
import com.webbook.springmvc.entities.Publisher;
import com.webbook.springmvc.service.AuthorService;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.GenreService;
import com.webbook.springmvc.service.PublisherService;



@Controller
@RequestMapping(value="/bookcontroller")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@Autowired
	private PublisherService publisherService;
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private GenreService genreService;

	@RequestMapping(value="/books",method=RequestMethod.GET)
	public ModelAndView getAll()
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookList");
		model.addObject("books", bookService.getAll());
		System.out.println("dau "+ bookService.getAll()+"duoi");
		return model;
	}
	
	@RequestMapping(value="/addBook",method=RequestMethod.GET)
	public ModelAndView addBook() 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", new Book());
		model.addObject("authorlist", authorService.getAll());
		model.addObject("genres", genreService.getAll());
		model.addObject("publishers", publisherService.getAll());
		model.addObject("mode", "EDIT");
		return model;
	}
	
	@RequestMapping(value="/book", method=RequestMethod.POST)
	public String saveBook(@ModelAttribute("book")Book book) 
	{
		book.author_id=authorService.get(book.author_id.getId());
		book.genre_id=genreService.get(book.genre_id.getId());
		book.publisher=publisherService.get(book.publisher.getId());
		System.out.println("book la: "+book);
		if(book.id==null)
		{
			bookService.add(book);
		}
		else
		{
			bookService.update(book);
		}
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/book",method=RequestMethod.GET)
	public ModelAndView getBookbyId(@RequestParam Long id,@RequestParam String mode) 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("bookDetail");
		model.addObject("book", bookService.get(id));
		if (mode.equalsIgnoreCase("EDIT")) {
			model.addObject("authorlist", authorService.getAll());
			model.addObject("authors", publisherService.getAll());
			model.addObject("publishers", publisherService.getAll());
			model.addObject("genres", genreService.getAll());
		}
		model.addObject("mode", mode);
		System.out.println(model.getModel());
		return model;
	}
	@RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE)
	public @ResponseBody void btnDelete(@PathVariable(value = "id") Long id) {
		System.out.println(id);
		bookService.delete(id);
	}
	
	@RequestMapping(value="/homeadmin")
	public String homeadmin()
	{
		return "redirect:/admin/view";
	}
	@RequestMapping(value="/adminauthor")
	public String GetAuthor()
	{
		return "redirect:/authorcontroller/authors";
	}
	@RequestMapping(value="/adminbook")
	public String GetBook()
	{
		return "redirect:/bookcontroller/books";
	}
	@RequestMapping(value="/adminpublisher")
	public String GetPublisher()
	{
		return "redirect:/publishercontroller/publishers";
	}
	@RequestMapping(value="/adminuser")
	public String Getuser()
	{
		return "redirect:/usercontroller/users";
	}
	@RequestMapping(value="/admingenre")
	public String GetGenre()
	{
		return "redirect:/genrecontroller/genres";
	}
}
