package com.webbook.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.webbook.springmvc.service.AuthorService;
import com.webbook.springmvc.service.BookService;
import com.webbook.springmvc.service.PublisherService;

@Controller
@RequestMapping(value="/")
public class ViewController {

	@Autowired
	private BookService bookService;
	
	@Autowired 
	private AuthorService authorService;
	
	@Autowired
	private PublisherService publisherService;
	
	
	@RequestMapping(value="/home",method=RequestMethod.GET)
	public ModelAndView getallBook() 
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("");//them viewname
		model.addObject("books", bookService.getAll());
		//neu can thi them vao khong can thi thoi
		//model.addObject("authors", authorService.getAll());
		//model.addObject("publishers", publisherService.getAll());
		return model;
	}
	
	@RequestMapping(value="/book",method=RequestMethod.GET)
	public ModelAndView getBookByID(@RequestParam(name="ID_BOOK")Long id)
	{
		ModelAndView model=new ModelAndView();
		model.setViewName("");//them vao ten viewname
		model.addObject("book", bookService.get(id));
		return model;
	}
	
	
}
