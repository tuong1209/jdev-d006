package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "book", catalog = "webbook")
public class Book implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long id;
	
	@Column(name = "Name", length = 50)
	public String name;
	
	@Column(name = "Price", length = 50)
	public Long price;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID")
	public Author author_id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "GENRE_ID", referencedColumnName = "ID")
	public Genre genre_id;

//	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
//	@JoinTable(name = "BOOK_PUBLISHER", joinColumns = { @JoinColumn(name = "ID") }, inverseJoinColumns = {
//			@JoinColumn(name = "ID_PUBLISHER") })
//	public List<Publisher> publisher = new ArrayList<Publisher>();
	@ManyToOne(fetch=FetchType.EAGER, cascade= CascadeType.PERSIST)
	@JoinColumn(name="PUBLISHER_ID", referencedColumnName="ID")
	public Publisher publisher;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Author getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(Author author_id) {
		this.author_id = author_id;
	}

	public Genre getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(Genre genre_id) {
		this.genre_id = genre_id;
	}

	/*public List<Publisher> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		this.publisher = publisher;
	}*/
	
	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", price=" + price + ", author_id=" + author_id + ", genre_id="
				+ genre_id + ", publisher=" + publisher + "]";
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
}
