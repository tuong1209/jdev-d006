package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "order_details", catalog = "webbook")
public class OrderDetails implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="order_id", referencedColumnName="ID")
	public Order order_id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="book_id", referencedColumnName="ID")
	public Book book;
	
	public int quantity;
	
	public int total_price;

	@Override
	public String toString() {
		return "OrderDetails [id=" + id + ", order_id=" + order_id + ", book=" + book + ", quantity=" + quantity
				+ ", total_price=" + total_price + "]";
	}
	
}

