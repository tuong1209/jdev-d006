package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.OrderDetails;

@Repository
public class OrderDetailsDao extends Dao<OrderDetails>{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<OrderDetails> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from OrderDetails").list();
	}
	@Override
	public OrderDetails get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (OrderDetails) session.get(OrderDetails.class, new Long(id));
	}
	@Override
	public OrderDetails add(OrderDetails orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(orderdetail);
		return orderdetail;
	}
	@Override
	public Boolean update(OrderDetails orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(orderdetail);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
	
	@Override
	public Boolean delete(OrderDetails orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != orderdetail) {
			try {
				session.delete(orderdetail);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		OrderDetails orderdetail = (OrderDetails) session.load(OrderDetails.class, new Long(id));
		if (null != orderdetail) {
			session.delete(orderdetail);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
