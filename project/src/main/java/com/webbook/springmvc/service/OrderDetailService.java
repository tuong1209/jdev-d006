package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.dao.OrderDetailsDao;
import com.webbook.springmvc.entities.OrderDetails;

@Transactional
@Service
public class OrderDetailService {
	
	@Autowired
	OrderDetailsDao orderdetailDAO;
	
	public List<OrderDetails> getAll(){
		return orderdetailDAO.getAll();
	}
	
	public OrderDetails get(Long id){
		return orderdetailDAO.get(id);
	}
	
	public OrderDetails add(OrderDetails t){
		return orderdetailDAO.add(t);
	}
	
	public Boolean update(OrderDetails t){
		return orderdetailDAO.update(t);
	}
	
	public Boolean delete(OrderDetails t){
		return orderdetailDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderdetailDAO.delete(id);
	}
}